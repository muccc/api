# MuCCC API

To run install [uv](https://docs.astral.sh/uv/) and execute:

    $ uv run muccc-api

If you're using Nix, you can use `nix-build` to build a package with all dependencies.
