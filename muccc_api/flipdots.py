import asyncio
import contextlib

from loguru import logger

from .FlipdotAPI.FlipdotMatrix import FlipdotMatrix
from .log import setup_logging
from .schleuse import SchleuseStateEnum, wait_for_schleuse_state

flipdotMatrix = FlipdotMatrix(("2a01:7e01:e003:8bef:222:f9ff:fe01:c65", 2323), (40, 16))


async def update(state: SchleuseStateEnum):
    flipdotMatrix.showText(f"\x01 state    {state.value}", linebreak=True, xPos=2, yPos=1)
    logger.debug(f"Flipped state: {state.value}")


def run():
    setup_logging()
    loop = asyncio.get_event_loop()
    loop.create_task(wait_for_schleuse_state(update))  # noqa: RUF006
    with contextlib.suppress(KeyboardInterrupt):
        loop.run_forever()
