from pydantic import BaseModel


class SpaceAPIState(BaseModel):
    open: bool | None
    message: str


class Location(BaseModel):
    address: str
    lat: float
    lon: float
    timezone: str


class Feed(BaseModel):
    url: str
    type: str


class SpaceAPIResponse(BaseModel):
    api: str = "0.13"
    api_compatibility: list[str] = ["14"]
    space: str = "MuCCC"
    logo: str = "https://muc.ccc.de/lib/tpl/muc3/images/muc3_klein.gif"
    url: str = "https://muc.ccc.de"
    location: Location = Location(
        address="Heßstrasse 90, München, Germany",
        lat=48.15367,
        lon=11.56078,
        timezone="Europe/Berlin",
    )
    contact: dict[str, str] = {
        "ml": "talk@lists.muc.ccc.de",
        "twitter": "@muccc",
        "mastodon": "@muccc@chaos.social",
        "irc": "ircs://irc.hackint.org/#muccc",
        "email": "info@muc.ccc.de",
        "mumble": "mumble://fasel.muc.ccc.de",
        "matrix": "#muccc:hackint.org",
    }
    issue_report_channels: list[str] = ["ml"]
    feeds: dict[str, Feed] = {
        "calendar": Feed(
            url="https://api.muc.ccc.de/events/public.ics",
            type="ical",
        ),
    }
    projects: list[str] = [
        "https://wiki.muc.ccc.de/projekte",
        "https://github.com/muccc",
        "https://gitlab.muc.ccc.de",
    ]
    state: SpaceAPIState
