# pyright: reportMissingTypeStubs=false, reportUnknownMemberType=false

import asyncio
import contextlib
import sys
from collections.abc import AsyncIterator
from typing import Annotated, Any

import uvicorn
from authlib.integrations.starlette_client import OAuth, OAuthError
from fastapi import Depends, FastAPI, Request, Response
from loguru import logger
from prometheus_fastapi_instrumentator import Instrumentator as PrometheusInstrumentator
from pydantic import BaseModel, ConfigDict, ValidationError
from pydantic_settings import CliApp
from starlette.middleware.sessions import SessionMiddleware
from starlette.responses import HTMLResponse, RedirectResponse

from ..FlipdotAPI.FlipdotMatrix import FlipdotMatrix
from ..log import setup_logging
from .bot import MatrixBot
from .settings import Settings

bot: MatrixBot | None = None
oauth: OAuth | None = None


async def get_bot() -> AsyncIterator[MatrixBot]:
    global bot
    if bot is not None:
        yield bot


async def get_oauth() -> AsyncIterator[OAuth]:
    global oauth
    if oauth is not None:
        yield oauth


@contextlib.asynccontextmanager
async def lifespan(_: FastAPI):
    if bot is None:
        logger.error("No MatrixBot instance")
        sys.exit(1)
    asyncio.create_task(bot.run(interval=15))  # noqa: RUF006
    yield


app = FastAPI(lifespan=lifespan, docs_url=None, redoc_url=None)

PrometheusInstrumentator().instrument(app).expose(app)


class MessageRequest(BaseModel):
    room_id: str
    message: str


class AuthRequest(BaseModel):
    user_id: str


@app.get("/")
async def homepage(request: Request, bot: Annotated[MatrixBot, Depends(get_bot)]):
    user = UserInfo.model_validate(request.session.get("user"))
    if user:
        space_members = await bot.get_room_members(bot.settings.internal_space_id)
        if space_members is None:
            return Response("Error fetching room members", status_code=500)
        joined = 'No - <a href="/invite">Invite</a>'
        for member in space_members:
            if user.matrix == member.user_id:
                joined = "Yes"
                break
        html = (
            f"<pre>Username: {user.preferred_username}</pre>"
            f"<pre>Email: {user.email}</pre>"
            f"<pre>Matrix: {user.matrix}</pre>"
            f"<pre>Groups: {user.groups}</pre>"
            f"<pre>Joined muCCC Space: {joined}</pre>"
            "<br />"
            '<a href="/logout">logout</a>'
        )
        return HTMLResponse(html)
    return RedirectResponse(url="/login")


@app.get("/invite")
async def invite(request: Request, bot: Annotated[MatrixBot, Depends(get_bot)]):
    """
    Invite muCCC members to space
    """
    user = request.session.get("user")
    if user:
        matrix = user.get("matrix")
        logger.debug(f"inviting {matrix} to space {bot.settings.internal_space_id}")
        await bot.invite_user(bot.settings.internal_space_id, matrix)
    return RedirectResponse(url="/")


@app.get("/login")
async def login(request: Request, oauth: Annotated[OAuth, Depends(get_oauth)]) -> Any:
    """
    Login muCCC members
    """
    redirect_uri = request.url_for("auth")
    logger.debug(f"request login: {redirect_uri}")

    return await oauth.authentik.authorize_redirect(request, redirect_uri)  # pyright: ignore


class UserInfo(BaseModel):
    model_config = ConfigDict()

    preferred_username: str
    matrix: str
    email: str
    groups: list[str]


@app.get("/auth")
async def auth(request: Request, oauth: Annotated[OAuth, Depends(get_oauth)]) -> Response:
    """
    Auth muCCC members
    """
    try:
        token = await oauth.authentik.authorize_access_token(request)  # pyright: ignore
        if token is None:
            return Response(status_code=400)
    except OAuthError:
        return Response(status_code=500)
    try:
        user = UserInfo.model_validate(token.get("userinfo", {}))
    except ValidationError:
        return Response(status_code=401)
    request.session["user"] = user.model_dump()
    return RedirectResponse(url="/")


@app.get("/logout")
async def logout(request: Request) -> HTMLResponse:
    """
    Logout muCCC members
    """
    request.session.clear()
    return HTMLResponse('<a href="/login">login</a>')


def run():
    setup_logging()
    settings = CliApp.run(Settings)

    flipdotZeile = FlipdotMatrix((str(settings.flipdot), 2323), (11 * 16, 20))

    global bot
    bot = MatrixBot(settings, flipdotZeile)

    global oauth
    oauth = OAuth()
    oauth.register(
        name="authentik",
        client_id=settings.oauth_client_id,
        client_secret=settings.oauth_client_secret,
        server_metadata_url=settings.oauth_metadata_url,
        client_kwargs={"scope": "openid email profile matrix"},
    )

    app.add_middleware(SessionMiddleware, secret_key=settings.session_secret_key)

    config = uvicorn.Config(app=app, host=settings.http_listen_host, port=settings.http_listen_port)
    server = uvicorn.Server(config)
    asyncio.run(server.serve())
