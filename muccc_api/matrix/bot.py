# pyright: reportMissingTypeStubs=false, reportUnknownMemberType=false

import asyncio
import json

import aiohttp
from loguru import logger
from nio import (
    AsyncClient,
    AsyncClientConfig,
    InviteMemberEvent,
    JoinedMembersResponse,
    LoginResponse,
    MatrixRoom,
    RoomGetStateEventError,
    RoomInviteResponse,
    RoomMember,
    RoomMessage,
    RoomMessageText,
    RoomTopicEvent,
    UploadResponse,
    re,
)
from nio.client.async_client import aiofiles
from pydantic import FilePath

from muccc_api.schleuse import SchleuseStateEmojiEnum, SchleuseStateEnum, SchleuseStateResponse

from .. import flipdots, wiki
from ..FlipdotAPI.FlipdotMatrix import FlipdotMatrix
from .settings import Settings


class MatrixBot:
    schleuse_state: SchleuseStateEnum | None = None
    api_event: wiki.Event | None = None
    last_topic: str | None = None
    extra_topic: str | None = None
    settings: Settings
    flipdotZeile: FlipdotMatrix

    def __init__(self, settings: Settings, flipdotZeile: FlipdotMatrix):
        self.settings = settings
        self.flipdotZeile = flipdotZeile

        client_config = AsyncClientConfig(
            max_limit_exceeded=0,
            max_timeouts=0,
            store_sync_tokens=True,
            encryption_enabled=False,
        )
        self.client = AsyncClient(
            settings.homeserver,
            settings.user_id,
            config=client_config,
        )

    async def update_topic(self, current_topic: str | None):
        schleuse_state, api_event = await self.check_api()
        optsep = " | " if self.extra_topic else ""
        new_topic = f"{schleuse_state} | {api_event.to_topic_str()}{optsep}{self.extra_topic or ''}"

        logger.debug(f"last_topic: {self.last_topic}")
        logger.debug(f"extra_topic: {self.extra_topic}")
        logger.debug(f"new_topic: {new_topic}")

        if current_topic is None:
            logger.debug(f"Resetting topic: {new_topic}")
            self.extra_topic = ""
            await self.change_topic(new_topic)
            return
        elif self.extra_topic is None and current_topic.startswith(new_topic):
            self.extra_topic = current_topic[len(new_topic) + 3 :]
            logger.debug(f"Recovered extra topic: {self.extra_topic}")
            self.last_topic = current_topic
            return
        elif self.extra_topic is None:
            self.extra_topic = ""

        if current_topic != new_topic:
            logger.debug(f"Updating topic: {new_topic}")
            await self.change_topic(new_topic)

    async def handle_topic_change(self, room: MatrixRoom, event: RoomTopicEvent | None):
        """
        Handle room topic change events.

        :param room: The room where the event occurred.
        :param event: The topic change event object.
        """
        if event is not None:
            read_marker_task = asyncio.create_task(
                self.client.room_read_markers(
                    room.room_id, event.event_id, read_event=event.event_id
                )
            )

            self.last_topic = event.topic

            # Ignore events initiated by the bot itself
            if event.sender == self.settings.user_id:
                logger.debug(f"Ignoring topic change event from the bot itself: {event.sender}")
            else:
                logger.info(
                    f"Topic in room {room.room_id} changed by {event.sender} to: {event.topic}"
                )
                await self.update_topic(event.topic)

            asyncio.gather(read_marker_task)

    async def get_room_members(self, room: str) -> list[RoomMember] | None:
        response = await self.client.joined_members(room)
        if isinstance(response, JoinedMembersResponse):
            return response.members
        else:
            logger.error(f"Failed to fetch joined members: {response}")
        return None

    async def fetch_current_topic(self) -> str | None:
        """
        Fetch the current topic of the room.
        """
        response = await self.client.room_get_state_event(
            room_id=self.settings.main_room_id,
            event_type="m.room.topic",
        )
        if isinstance(response, RoomGetStateEventError):
            logger.warning(f"Cannot get room state: {response}")
            return None
        if response and "topic" in response.content:
            room_topic: str = response.content["topic"]  # pyright: ignore[reportUnknownVariableType]
            logger.debug(f"current topic from room: {room_topic}")
            return room_topic  # pyright: ignore[reportUnknownVariableType]
        else:
            logger.info("No current topic found.")
            return None

    async def change_topic(self, new_topic: str):
        """
        Change the topic of a Matrix room.

        :param new_topic: The new topic to set.
        """
        try:
            response = await self.client.room_put_state(
                room_id=self.settings.main_room_id,
                event_type="m.room.topic",
                content={"topic": new_topic},
            )
            if response:
                logger.info(f"Topic changed to: {new_topic}")
                self.last_topic = new_topic
            else:
                logger.info("Failed to change the topic.")
        except Exception as e:  # noqa: BLE001
            logger.warning(f"Error changing topic: {e}")

    async def check_api(self) -> tuple[str | None, wiki.Event]:
        """
        Function to dynamically fetch updates from API.
        """
        async with aiohttp.ClientSession(self.settings.api_base_url) as session:
            async with session.get("/schleuse.json") as resp_schleuse:
                schleuse_reponse = await resp_schleuse.json()
                new_schleuse_state = SchleuseStateResponse.model_validate_json(
                    json.dumps(schleuse_reponse)
                ).state

            async with session.get("/events/upcoming?limit=1") as resp_event:
                new_state_events = await resp_event.json()
                next_event: wiki.Event = wiki.Event.model_validate_json(
                    json.dumps(new_state_events[0])
                )

        if self.schleuse_state != new_schleuse_state and new_schleuse_state is not None:
            self.schleuse_state = new_schleuse_state
            await flipdots.update(self.schleuse_state)

        return (
            f"{SchleuseStateEmojiEnum.from_schleuse_state(self.schleuse_state)} "
            f"{self.schleuse_state}",
            next_event,
        )

    async def set_display_name(self, displayname: str):
        """
        Set the display name of the bot.

        :param display_name: Display name to set.
        """
        try:
            response = await self.client.set_displayname(displayname)
            if response:
                logger.info(f"Display name set to: {displayname}")
            else:
                logger.info("Failed to set display name.")
        except Exception as e:  # noqa: BLE001
            logger.warning(f"Error setting display name: {e}")

    async def set_avatar(self, image_path: FilePath):
        """
        Set the bot's avatar.

        :param image_path: The local path to the avatar image file.
        """
        try:
            mime_type = "image/jpeg"
            file_stat = image_path.stat()

            async with aiofiles.open(image_path, "r+b") as f:
                resp = await self.client.upload(
                    f,
                    content_type=mime_type,  # image/jpeg
                    filename=image_path.name,
                    filesize=file_stat.st_size,
                )

            if isinstance(resp[0], UploadResponse):
                logger.info(f"Image uploaded successfully. MXC URI: {resp[0].content_uri}")
                # Set the bot's avatar using the uploaded image's MXC URI
                logger.info("Setting avatar...")
                await self.client.set_avatar(resp[0].content_uri)
                logger.info("Avatar set successfully.")
            else:
                logger.info(f"Failed to upload image. Failure response: {resp}")
        except Exception as e:  # noqa: BLE001
            logger.debug(f"Failed to set avatar: {e}")

    async def handle_message(self, room: MatrixRoom, event: RoomMessageText):
        """
        Handle incoming messages in the room.

        :param room: The room object where the event occurred.
        :param event: The event object containing the message.
        """
        # Extract the message body and the sender
        message = event.body
        sender = event.sender

        logger.debug(f"Received message from {sender} in {room.room_id}: {message}")

        read_marker_task = asyncio.create_task(
            self.client.room_read_markers(room.room_id, event.event_id, read_event=event.event_id)
        )

        # Check if the message starts with the "!topic" command
        match_topic = re.match(r"!topic (.+)", message)
        if match_topic:
            new_extra_topic = match_topic.group(1).strip()
            logger.info(f"Command received to change extra topic to: {new_extra_topic}")

            # Ensure the command is from the expected room
            if room.room_id == self.settings.main_room_id:
                self.extra_topic = new_extra_topic
                await self.update_topic(self.last_topic)

        match_alarm = re.match(r".*alarm.*", message, re.IGNORECASE)
        if match_alarm and room.room_id == self.settings.main_room_id:
            logger.info(f"alarm: {message}")
            self.flipdotZeile.showText(message, linebreak=True, xPos=1, yPos=0)
            await self.send_reaction_to_room(room.room_id, "🚨", event)

        asyncio.gather(read_marker_task)

    async def send_matrix_message(self, room: str, message: str):
        """
        Send outgoing messages in the room.

        :param room: The room object where the event occurred.
        :param message: The actual message.
        """
        await self.client.room_send(
            room_id=room,
            message_type="m.room.message",
            content={"msgtype": "m.text", "body": message},
        )

    async def send_reaction_to_room(
        self,
        room_id: str,
        message: str,
        event: RoomMessage,
    ):
        content = {
            "m.relates_to": {
                "rel_type": "m.annotation",
                "event_id": event.event_id,
                "key": message,
            }
        }

        await self.client.room_send(
            room_id,
            "m.reaction",
            content,
            ignore_unverified_devices=True,
        )

    async def handle_invite(self, room: MatrixRoom, event: InviteMemberEvent):
        """
        Handle room invitations and automatically join the room.

        :param room: The room object for the invite.
        :param event: The event object for the invite.
        """
        if event.state_key == self.client.user_id:
            logger.info(f"Bot invited to room {room.room_id} by {event.sender}. Joining...")
            response = await self.client.join(room.room_id)
            if response:
                logger.info(f"Joined room {room.room_id}.")

    async def invite_user(self, room: str, user: str):
        """
        Invite user to room/space

        :param room: The room/space id user should be invited to
        :param user: User id to invite to room/space
        """

        invite_response = await self.client.room_invite(
            room_id=room,
            user_id=user,
        )
        if isinstance(invite_response, RoomInviteResponse):
            logger.info(f"User {user} invited successfully to room/space {room}")
        else:
            logger.info(f"Failed to invite user: {invite_response}")

    async def login(self):
        """
        Log in to the Matrix server.
        """
        try:
            response = await self.client.login(self.settings.password)
            if isinstance(response, LoginResponse):
                logger.info(f"Logged in as {self.settings.user_id}")
                await self.client.join(self.settings.main_room_id)
            else:
                logger.info(f"Login failed: {response}")
                exit(1)
        except Exception as e:  # noqa: BLE001
            logger.warning(f"Error loging in: {e}")
            exit(1)

    async def listen_to_events(self):
        """
        Listen for room events and handle them.
        """
        # Register an event listener for room messages
        self.client.add_event_callback(self.handle_message, RoomMessageText)  # pyright: ignore[reportArgumentType]

        # Listen for topic changes
        self.client.add_event_callback(self.handle_topic_change, RoomTopicEvent)  # pyright: ignore[reportArgumentType]

        # Listen for space/room invites
        self.client.add_event_callback(self.handle_invite, InviteMemberEvent)  # pyright: ignore[reportArgumentType]

        # Sync loop
        logger.info("Listening for events...")
        while True:
            await self.client.sync_forever(timeout=30000)

    async def poll_for_updates(self, interval: int = 10):
        """
        Poll for topic updates from an external function.

        :param interval: Time in seconds between topic checks.
        """
        while True:
            await self.update_topic(self.last_topic)
            await asyncio.sleep(interval)

    async def run(self, interval: int = 10):
        """
        Run the bot with both event listening and periodic polling.

        :param interval: Time in seconds between topic checks.
        """
        await self.login()
        await self.client.sync()

        last_topic = await self.fetch_current_topic()
        await self.update_topic(last_topic)
        logger.info(f"Initial topic: {self.last_topic}")

        await asyncio.gather(self.listen_to_events(), self.poll_for_updates(interval))
