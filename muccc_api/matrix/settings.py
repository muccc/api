from pathlib import Path

from pydantic import Field, IPvAnyAddress
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_prefix="", env_file=".env", env_file_encoding="utf-8")

    homeserver: str = Field("https://matrix.muc.ccc.de")
    user_id: str = Field("@maggus:muc.ccc.de")
    password: str = Field()
    main_room_id: str = Field(
        "!uciUIKjQUAmUEKasZk:muc.ccc.de"  # #bot-test:muc.ccc.de
    )
    internal_space_id: str = Field(
        "!rzoVREkZYKivCqNdBM:muc.ccc.de"  # bot-test-space
    )
    api_base_url: str = Field("http://localhost:8020")
    session_secret_key: str = Field("CHANGEME")
    sync_token_file: Path = Field("sync_token.json")

    display_name: str = Field("Maggus")
    flipdot: IPvAnyAddress = Field("2a01:7e01:e003:8bef:ba27:ebff:fe89:4cd2")

    http_listen_host: str = Field("::1")
    http_listen_port: int = Field(8000)
    oauth_metadata_url: str = Field(
        "https://auth.muc.ccc.de/application/o/members-auth/.well-known/openid-configuration"
    )
    oauth_client_id: str = Field()
    oauth_client_secret: str = Field()
